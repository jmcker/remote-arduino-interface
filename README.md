# Remote Arduino Interface #

This project was originally designed for a theatre production. It is designed to remotely control an Arduino board connected to a large number of LEDs. 
The program, however, is much more versatile than simple control or even simply LEDs. It serves as a customizable way to interface with both a host operating system and an Arduino.

v4.0

For more detailed installation and setup instructions, please contact me. Hopefully I'll get a chance to provide better documentation soon.

## System Requirements ##
* Python 2.x
* [nanpy](https://pypi.python.org/pypi/nanpy)


## Contact ##

* Jack McKernan
* [jmcker@outlook.com](mailto:jmcker@outlook.com)